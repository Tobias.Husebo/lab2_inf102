package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if (index<0 || index>=size()){
			throw new IndexOutOfBoundsException();
		} else {
			return (T) elements[index];
		}
	}
	
	@Override
	public void add(int index, T element) {
		if (index<0){
			throw new IndexOutOfBoundsException("Cannot add negative index");
		} else if (index > size()){
			throw new IndexOutOfBoundsException("Cannot add index " + index + "," +
												 "last known index is " + (size()-1));
		} else if (index == size()) {
			if (size() == elements.length-1){
				int newSize = elements.length*2;
				elements = Arrays.copyOf(elements, newSize);
			}
			elements[index] = element;
			n++;
		} else {
			if (size() == elements.length-1){
				int newSize = elements.length*2;
				elements = Arrays.copyOf(elements, newSize);
			}
			for (int i = index; i<size(); i++) {
				T OldElem = (T) elements[i];
				elements[i] = element;
				element = OldElem;
			}
			elements[size()] = element;
			n++;
		}
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}